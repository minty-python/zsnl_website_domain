import mock
from zsnl_domain_website import CommandBase, JiraRepositoryBase, QueryBase


class TestDomainWebsite:
    def test_command_base(self):
        CommandBase(repository_factory=None, context=None, user_uuid=None)

    def test_query_base(self):
        QueryBase(repository_factory=None, context=None, user_uuid=None)


class MockInfrastructureFactory:
    def __init__(self, mock_infra):
        self.infrastructure = mock_infra

    def get_infrastructure(self, context, infrastructure_name):
        return self.infrastructure[infrastructure_name]


class TestJiraRepositoryBase:
    def test_jira_repository_base(self):
        mock_infra = MockInfrastructureFactory(
            mock_infra={"jira": mock.MagicMock()}
        )
        jira_repo_base = JiraRepositoryBase(
            infrastructure_factory=mock_infra, context=None
        )
        jira_repo_base._get_infrastructure("jira")
