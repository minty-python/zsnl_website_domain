from unittest import mock
from zsnl_domain_website.infrastructure.jira import JiraInfrastructure
from zsnl_domain_website.infrastructure.jira.jira import JIRA


class TestJiraInfrastructure:
    def setup(self):
        self.config = {
            "jira": {
                "server": {
                    "options": {
                        "server": "https://host.atlassian.net/",
                        "agile_rest_path": "test_agile_path",
                    },
                    "oauth": {
                        "access_token": "12345ABC",
                        "access_token_secret": "12345ABC",
                        "consumer_key": "dev",
                        "key_cert_file": "private_key_file",
                    },
                    "timeout": 5,
                },
                "cache": {
                    "cache_max_length": "100",
                    "cache_max_age_seconds": "4800",
                },
                "board": {
                    "board_name": "XYZ",
                    "board_id": "1234",
                    "project_id": "1234",
                    "max_results": "1000",
                    "fields": {"bugs": "issuetype,project,sprint"},
                },
            }
        }

    def test_jira_infrastructue(self):
        jira_infra = JiraInfrastructure()
        jira_infra.connection = "jira connection"
        jira_connection = jira_infra(config=self.config)
        assert jira_connection == "jira connection"

    @mock.patch("jira.JIRA")
    @mock.patch("expiringdict.ExpiringDict")
    @mock.patch("builtins.open", mock.mock_open(read_data="data"))
    def test_jira_infrastructue_no_connection(self, mockExpringDict, mockJIRA):
        jira_infra = JiraInfrastructure()
        jira_connection = jira_infra(config=self.config)
        assert isinstance(jira_connection, JIRA)
