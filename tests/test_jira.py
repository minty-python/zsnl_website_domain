import jira
import pytest
import requests
from minty.exceptions import NotFound
from unittest import mock
from zsnl_domain_website.infrastructure.jira.jira import JIRA


class TestJIRA:
    @mock.patch("jira.JIRA")
    @mock.patch("expiringdict.ExpiringDict")
    @mock.patch("builtins.open", mock.mock_open(read_data="data"))
    def setup(self, mockExpringDict, mockJira):
        config = {
            "jira": {
                "server": {
                    "options": {
                        "server": "https://host.atlassian.net/",
                        "agile_rest_path": "test_agile_path",
                    },
                    "oauth": {
                        "access_token": "12345ABC",
                        "access_token_secret": "12345ABC",
                        "consumer_key": "dev",
                        "key_cert_file": "private_key_file",
                    },
                    "timeout": 5,
                },
                "cache": {
                    "cache_max_length": "100",
                    "cache_max_age_seconds": "4800",
                },
                "board": {
                    "board_name": "XYZ",
                    "board_id": "1234",
                    "project_id": "1234",
                    "max_results": "1000",
                    "fields": {
                        "bugs": "issuetype,status",
                        "bug_details": "issuetype,status,created_at",
                        "issues": "issuetype, key, status, public_description, public_summary, epic, fixVersions",
                    },
                },
            }
        }
        self.jira = JIRA(
            server_config=config["jira"]["server"],
            board_config=config["jira"]["board"],
            cache_config=config["jira"]["cache"],
        )
        self.jira.jira = mockJira

        self.mock_epics = [mock.MagicMock(), mock.MagicMock()]
        self.mock_epics[0].configure_mock(
            key="XXXX-1",
            fields=mock.MagicMock(
                issuetype=mock.MagicMock(name="Epic"),
                customfield_10009="Epic-1",
            ),
        )
        self.mock_epics[0].configure_mock(
            key="XXXX-2",
            fields=mock.MagicMock(
                issuetype=mock.MagicMock(name="Epic"),
                customfield_10009="Epic-2",
            ),
        )

        self.mock_issues = [mock.MagicMock()]
        self.mock_issues[0].configure_mock(
            key="XXXX-3",
            fields=mock.MagicMock(
                issuetype=mock.MagicMock(name="Bug"),
                customfield_11300="description_1",
                customfield_11710="summary_1",
                status=mock.MagicMock(name="open"),
                customfield_10008="XXXX-3",
                fixVersions=[
                    mock.MagicMock(name="release_1"),
                    mock.MagicMock(name="release_2"),
                ],
            ),
        )

        self.mock_releases = [mock.MagicMock(), mock.MagicMock()]
        self.mock_releases[0].configure_mock(
            name="Release-1",
            releaseDate="2020-07-10",
            startDAte="20202-07-07",
            released=True,
            archived=False,
        )
        self.mock_releases[0].configure_mock(
            name="Release-2",
            releaseDate="2020-05-10",
            startDAte="20202-05-05",
            released=True,
            archived=False,
        )

    def test_get_hidden_epics(self):
        self.jira.jira.search_issues.return_value = self.mock_epics

        res = self.jira.get_hidden_epics()
        assert res == self.mock_epics

        # When JIRA gives timeout error
        self.jira.jira.search_issues.side_effect = (
            requests.exceptions.ReadTimeout()
        )
        with pytest.raises(TimeoutError):
            self.jira.get_hidden_epics()

        # When JIRA can not find epics
        self.jira.jira.search_issues.side_effect = jira.exceptions.JIRAError
        with pytest.raises(NotFound):
            self.jira.get_hidden_epics()

    @mock.patch.object(JIRA, "get_hidden_epics")
    def test_get_issues(self, mock_hidden_epics):
        self.jira.jira.search_issues.return_value = self.mock_issues
        mock_hidden_epics.return_value = [self.mock_epics[0]]

        res = self.jira.get_issues()
        assert len(res) == 50
        assert res[0] == self.mock_issues[0]

        # When JIRA gives timeout error
        self.jira.jira.search_issues.side_effect = (
            requests.exceptions.ReadTimeout()
        )
        with pytest.raises(TimeoutError):
            self.jira.get_issues()

        # When JIRA can not find issues
        self.jira.jira.search_issues.side_effect = jira.exceptions.JIRAError
        with pytest.raises(NotFound):
            self.jira.get_issues()

    def test_get_epics(self):
        self.jira.jira.search_issues.return_value = self.mock_epics

        res = self.jira.get_epics()
        assert res == self.mock_epics

        # When JIRA gives timeout error
        self.jira.jira.search_issues.side_effect = (
            requests.exceptions.ReadTimeout()
        )
        with pytest.raises(TimeoutError):
            self.jira.get_epics()

        # When JIRA can not found epics
        self.jira.jira.search_issues.side_effect = jira.exceptions.JIRAError
        with pytest.raises(NotFound):
            self.jira.get_epics()

    def test_get_releases(self):
        self.jira.jira.project_versions.return_value = self.mock_releases

        res = self.jira.get_releases()
        assert res == self.mock_releases

        # When JIRA gives timeout error
        self.jira.jira.project_versions.side_effect = (
            requests.exceptions.ReadTimeout()
        )
        with pytest.raises(TimeoutError):
            self.jira.get_releases()

        # When JIRA can not find releases
        self.jira.jira.project_versions.side_effect = jira.exceptions.JIRAError
        with pytest.raises(NotFound):
            self.jira.get_releases()
