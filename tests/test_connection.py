import multiprocessing
import pytest
from unittest import mock
from zsnl_domain_website.infrastructure.jira.connection import (
    get_jira_connection,
    make_jira_connection,
)


class TestConnection:
    @mock.patch(
        "jira.JIRA", side_effect=multiprocessing.context.TimeoutError()
    )
    @mock.patch("builtins.open", mock.mock_open(read_data="data"))
    def test_get_jira_connection(self, mockJira):
        with pytest.raises(TimeoutError):
            get_jira_connection(
                options="options",
                oauth={
                    "access_token": "12345ABC",
                    "access_token_secret": "12345ABC",
                    "consumer_key": "dev",
                    "key_cert_file": "private_key_file",
                },
                timeout=1,
            )

    @mock.patch("jira.JIRA")
    @mock.patch("builtins.open", mock.mock_open(read_data="data"))
    def test_make_jira_connection(self, mockJira):
        mockJira.return_value = "jira_connection_established"
        res = make_jira_connection(
            options="options",
            oauth={
                "access_token": "12345ABC",
                "access_token_secret": "12345ABC",
                "consumer_key": "dev",
                "key_cert_file": "private_key_file",
            },
            timeout="timeout",
        )

        open.assert_called_with("private_key_file", "r")
        assert open("private_key_file").read() == "data"
        assert res == "jira_connection_established"
