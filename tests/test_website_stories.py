import datetime
import json
import pytest
from minty.cqrs.test import TestBase
from minty.exceptions import NotFound
from unittest import mock
from zsnl_domain_website import website

today = datetime.datetime.now().strftime("%Y-%m-%d")
test_data = {
    "issues": [
        {
            "type": "Bug",
            "public_description": "description",
            "id": "XXX-1",
            "public_summary": "summary",
            "epic": "XXX-3",
            "releases": ["release_1"],
            "status": "to-do",
        },
        {
            "type": "Story",
            "public_description": "",
            "id": "XXX-2",
            "public_summary": "summary",
            "epic": "XXX-3",
            "releases": ["release_1"],
            "status": "done",
        },
        {
            "type": "Story",
            "public_description": "issue with no releases",
            "id": "XXX-4",
            "public_summary": "summary",
            "epic": "XXX-3",
            "releases": [],
            "status": "to-do",
        },
    ],
    "releases": [
        {
            "id": "release_1",
            "release_date": today,
            "start_date": today,
            "status": "released",
        },
        {
            "id": "release_2",
            "release_date": today,
            "start_date": today,
            "status": "archived",
        },
    ],
    "epics": [{"id": "XXX-3", "name": "Epic-1"}],
}


class Test_Update_Cache(TestBase):
    """Test update cache."""

    def setup(self):
        self.jira = mock.MagicMock()
        self.load_command_instance(website, inframocks={"jira": self.jira})

        self.mock_epics = [mock.MagicMock()]
        self.mock_epics[0].configure_mock(
            key="XXX-1", fields=mock.MagicMock(customfield_10009="Epic-1",),
        )

        self.mock_issues = [mock.MagicMock()]
        mock_issuetype = mock.MagicMock()
        mock_issuetype.configure_mock(name="Bug")

        mock_issuestatus = mock.MagicMock()
        mock_issuestatus.configure_mock(name="open")

        mock_issuerelease = mock.MagicMock()
        mock_issuerelease.configure_mock(name="release_1")

        self.mock_issues[0].configure_mock(
            key="XXXX-3",
            fields=mock.MagicMock(
                issuetype=mock_issuetype,
                customfield_11300="description",
                customfield_11710="summary",
                status=mock_issuestatus,
                customfield_10008="XXX-1",
                fixVersions=[mock_issuerelease],
            ),
        )

        self.mock_releases = [
            mock.MagicMock(),
            mock.MagicMock(),
            mock.MagicMock(),
        ]
        self.mock_releases[0].configure_mock(
            name="Release-1",
            releaseDate="2020-07-07",
            startDate="2020-07-07",
            released=True,
            archived=False,
        )
        self.mock_releases[1].configure_mock(
            name="Release-2",
            releaseDate="2020-05-05",
            startDate="20202-05-05",
            released=False,
            archived=False,
        )
        self.mock_releases[2].configure_mock(
            name="Release-3",
            releaseDate="2020-03-03",
            startDate="2020-03-03",
            released=False,
            archived=True,
        )

    @mock.patch("zsnl_domain_website.os")
    @mock.patch("zsnl_domain_website.open")
    def test_update_cache(self, mock_builtin_open, mock_os):

        self.jira.get_releases.return_value = self.mock_releases
        self.jira.get_issues.return_value = self.mock_issues
        self.jira.get_epics.return_value = self.mock_epics

        self.cmd.update_cache()

        mock_builtin_open.assert_called_once()
        mock_os.rename.assert_called_once()


class TestAsUserGetIssues(TestBase):
    """Test get_issues"""

    def setup(self):

        self.jira = mock.MagicMock()
        self.load_query_instance(website, inframocks={"jira": self.jira})
        self.load_command_instance(website, inframocks={"jira": self.jira})

    @mock.patch(
        "zsnl_domain_website.open",
        mock.mock_open(read_data=json.dumps(test_data)),
    )
    def test_get_issues(self):
        issues = self.qry.get_issues()

        assert issues[0].id == test_data["issues"][0]["id"]
        assert issues[0].type == test_data["issues"][0]["type"]

    @mock.patch(
        "zsnl_domain_website.open",
        mock.mock_open(read_data=json.dumps(test_data)),
    )
    def test_get_issues_with_filter_params(self):
        issues = self.qry.get_issues(
            filter_params={"relationships.release.id": "release_1"}
        )

        for issue in issues:
            assert "release_1" in issue.releases

    @mock.patch("zsnl_domain_website.open")
    def test_get_issues_with_error(self, mock_builtin_open):
        mock_builtin_open.side_effect = FileNotFoundError
        with pytest.raises(NotFound) as excinfo:
            self.qry.get_issues()
        assert excinfo.value.args == (
            "Page is broken. Try again after 30 mintues.",
        )


class TestAsUserGetReleases(TestBase):
    def setup(self):

        self.jira = mock.MagicMock()
        self.load_query_instance(website, inframocks={"jira": self.jira})
        self.load_command_instance(website, inframocks={"jira": self.jira})

    @mock.patch(
        "zsnl_domain_website.open",
        mock.mock_open(read_data=json.dumps(test_data)),
    )
    def test_get_releases(self):
        releases = self.qry.get_latest_releases()

        assert releases[0].id == test_data["releases"][0]["id"]
        assert releases[0].start_date == test_data["releases"][0]["start_date"]
        assert (
            releases[0].release_date
            == test_data["releases"][0]["release_date"]
        )
        assert releases[0].status == test_data["releases"][0]["status"]

    @mock.patch("zsnl_domain_website.open")
    def test_get_releases_with_error(self, mock_builtin_open):
        mock_builtin_open.side_effect = FileNotFoundError
        with pytest.raises(NotFound) as excinfo:
            self.qry.get_latest_releases()
        assert excinfo.value.args == (
            "Page is broken. Try again after 30 mintues.",
        )


class TestAsUserSearchIssuesFromLatestReleases(TestBase):
    """Test search_issues from latest releases"""

    def setup(self):

        self.jira = mock.MagicMock()
        self.load_query_instance(website, inframocks={"jira": self.jira})
        self.load_command_instance(website, inframocks={"jira": self.jira})

    @mock.patch(
        "zsnl_domain_website.open",
        mock.mock_open(read_data=json.dumps(test_data)),
    )
    def test_search_issues(self):
        # Search by id
        issues = self.qry.search_issues(keyword="XXX-1")
        assert len(issues) == 1
        assert issues[0].id == test_data["issues"][0]["id"]
        assert issues[0].type == test_data["issues"][0]["type"]

        # Search by summary
        issues = self.qry.search_issues(keyword="summ")
        assert len(issues) == 2
        assert issues[0].id == test_data["issues"][0]["id"]
        assert issues[0].type == test_data["issues"][0]["type"]

        # Search by epic
        issues = self.qry.search_issues(keyword="Epic")
        assert len(issues) == 2
        assert issues[0].id == test_data["issues"][0]["id"]
        assert issues[0].type == test_data["issues"][0]["type"]

        issues = self.qry.search_issues(keyword="release_2")
        assert len(issues) == 0

        # Search by id with or without capital letters
        issues = self.qry.search_issues(keyword="Xxx-1")
        assert len(issues) == 1
        assert issues[0].id == test_data["issues"][0]["id"]
        assert issues[0].type == test_data["issues"][0]["type"]

    @mock.patch("zsnl_domain_website.open")
    def test_search_issues_with_error(self, mock_builtin_open):
        mock_builtin_open.side_effect = FileNotFoundError
        with pytest.raises(NotFound) as excinfo:
            self.qry.search_issues(keyword="XXX-1")
        assert excinfo.value.args == (
            "Page is broken. Try again after 30 mintues.",
        )


class TestAsUserSearchIssuesGlobally(TestBase):
    """Test search issues globally(not just within latest releases)"""

    def setup(self):
        self.jira = mock.MagicMock()
        self.load_query_instance(website, inframocks={"jira": self.jira})
        self.load_command_instance(website, inframocks={"jira": self.jira})

    @mock.patch(
        "zsnl_domain_website.open",
        mock.mock_open(read_data=json.dumps(test_data)),
    )
    def test_search_issues_globally(self):

        # Search by Epic
        issues = self.qry.search_issues(
            keyword="Epic", only_in_latest_releases=False
        )
        assert len(issues) == 3
        assert issues[0].id == test_data["issues"][0]["id"]
        assert issues[0].type == test_data["issues"][0]["type"]

        # Search by description
        issues = self.qry.search_issues(
            keyword="issue with no releases", only_in_latest_releases=False
        )
        assert len(issues) == 1
        assert issues[0].id == test_data["issues"][2]["id"]
        assert issues[0].type == test_data["issues"][2]["type"]
