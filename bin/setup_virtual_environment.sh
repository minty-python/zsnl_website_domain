#!/usr/bin/env bash

set -euo pipefail

PKG_NAME="zsnl_domain_website"
PYTHON_VERSION="3.7"
PYTHON_COMMAND="python${PYTHON_VERSION}"
VENV_ROOT="${MINTY_VENV_ROOT:-${HOME}/.pyenv}"

install_requirements() {
    for reqs in requirements/*.txt; do
        pip install -r "${reqs}"
    done

    pip install --no-deps -e .
}

install_venv() {
    command -v "${PYTHON_COMMAND}" >/dev/null 2>&1 || \
        { echo >&2 "'${PYTHON_COMMAND}' is required, but it's not installed.  Aborting."; exit 1; }

    VENV_PATH="${VENV_ROOT}/${PKG_NAME}"

    echo "Installing virtual environment in '${VENV_PATH}'"
    "${PYTHON_COMMAND}" -m venv "${VENV_PATH}"

    # Activate the new environment
    source "${VENV_PATH}/bin/activate"

    install_requirements

    echo
    echo "You can activate the virtual environment by running:"
    echo "source ${VENV_PATH}/bin/activate"
}

DIY_instructions() {
    echo
    echo "# For development, do not forget to create your virtual env, requirements and egg:"
    echo "cd zsnl-domain-website"
    echo "${PYTHON_COMMAND} -m venv ${VENV_ROOT}/${PKG_NAME}"
    echo "source ${VENV_ROOT}/${PKG_NAME}/bin/activate"
    for reqs in requirements/*.txt; do
        echo "pip install -r \"${reqs}\""
    done
    echo "pip install --no-deps -e ."
}

interactive_mode() {
    while true; do
        echo "Install virtual env in directory '${VENV_ROOT}/${PKG_NAME}'?"
        echo "(you can override the path by setting \$MINTY_VENV_ROOT)"
        read -r -p "[Y]es or [N]o): " yn
        case $yn in
            [Yy]* ) install_venv; break;;
            [Nn]* ) DIY_instructions; exit;;
            * ) echo "Please answer yes or no.";;
        esac
    done
}

if [ "$#" != 0 ]; then
    for arg in "$@"; do
        if [ "$arg" == "--interactive" ] || [ "$arg" == "-i" ]; then
            interactive_mode
        else
            echo "Argument '$arg' not valid"
        fi
    done
else
    install_venv
fi

exit 0
