#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/.."
cd $DIR

mkdir -p $DIR/docs/generated
cp $DIR/{README,CONTRIBUTORS,CONTRIBUTING}.rst $DIR/docs/generated/
cp $DIR/docs/conf.py $DIR/docs/*.rst $DIR/docs/generated/
if [ "$1" == "test" ]; then
  mkdir -p /tmp/docs && sphinx-apidoc -e -f -o docs/generated zsnl_domain_website && sphinx-build -j 4 -a docs/generated /tmp/docs
else
  sphinx-apidoc -e -f -o docs/generated zsnl_domain_website && sphinx-build -j 4 -a docs/generated docs/output
fi
