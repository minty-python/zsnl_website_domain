import jira
import multiprocessing
from multiprocessing.pool import ThreadPool


def make_jira_connection(options, oauth, timeout):
    """Construct a JIRA client instance.

    :param options: Specify the server and properties this jira client will use.
    :type options: dict
    :param basic_auth: A tuple of username and password to use when establishing a session via HTTP BASIC authentication.
    :type basic_auth: tuple
    :param timeout: Set a read/connect timeout for the underlying calls to JIRA
    :type timeout:  int
    :return:  An instance of JIRA client
    :rtype: JIRA
    """
    key_cert_file = oauth["key_cert_file"]
    with open(key_cert_file, "r") as key_cert_file:
        key_cert_data = key_cert_file.read()
    oauth["key_cert"] = key_cert_data
    return jira.JIRA(options=options, oauth=oauth, timeout=timeout)


def get_jira_connection(options, oauth, timeout):
    """Get  JIRA client instance.

    :param options: Specify the server and properties this jira client will use.
    :type options: dict
    :param basic_auth: A tuple of username and password to use when establishing a session via HTTP BASIC authentication.
    :type basic_auth: tuple
    :param timeout: Set a read/connect timeout for the underlying calls to JIRA
    :type timeout: int
    :raises TimeoutError: In case of server connection timeout
    :return: An instance of JIRA client
    :rtype: JIRA
    """

    pool = ThreadPool(1)
    p = pool.apply_async(make_jira_connection, (options, oauth, timeout))
    try:
        return p.get(timeout=timeout)
    except multiprocessing.context.TimeoutError as e:
        raise TimeoutError("Jira server connection timeout") from e
