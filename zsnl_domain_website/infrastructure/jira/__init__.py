from .jira import JIRA
from minty import Base
from threading import Lock


class JiraInfrastructure(Base):
    """ Infrastructure for JIRA interaction """

    def __init__(self):
        """Initialize JiraInfrastructure """
        self.connection = None
        self.thread_lock = Lock()

    def __call__(self, config: dict):
        """Create an instance of JIRA object with specified configuration.Clients interact with JIRA by this object and calling its methods.

        Authentication is handled with the ``basic_auth`` argument. If authentication is supplied (and is
        accepted by JIRA), the client will remember it for subsequent requests.

        :param config: Configuration
        :type config: dict
        :return: CachedJIRA
        :rtype: CachedJIRA
        """
        server_config = config["jira"]["server"]
        cache_config = config["jira"]["cache"]
        board_config = config["jira"]["board"]
        with self.thread_lock:
            if not self.connection:
                self.connection = JIRA(
                    server_config=server_config,
                    board_config=board_config,
                    cache_config=cache_config,
                )
            return self.connection
