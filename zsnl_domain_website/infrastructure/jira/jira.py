import jira
import requests
from minty import Base
from minty.exceptions import NotFound
from zsnl_domain_website.infrastructure.jira.connection import (
    get_jira_connection,
)


class JIRA(Base):
    def __init__(
        self, server_config=None, board_config=None, cache_config=None
    ):
        """Initialize JIRA for queries"""

        self.jira = get_jira_connection(
            options=server_config["options"],
            oauth=server_config["oauth"],
            timeout=int(server_config["timeout"]),
        )
        self.board_id = int(board_config["board_id"])
        self.project_id = int(board_config["project_id"])
        self.max_results = int(board_config["max_results"])
        self.fields = board_config["fields"]

    def get_releases(self):
        """Get project releases for an year.

        :raises TimeoutError: When jira server connection timeout
        :raises NotFound: When jira server cannot find project_versions
        :return: List of releases
        :rtype: List[Version]
        """

        try:
            return self.jira.project_versions(project=str(self.project_id))
        except requests.exceptions.ReadTimeout as e:
            raise TimeoutError("Jira server connection timeout") from e
        except jira.exceptions.JIRAError as e:
            raise NotFound(e) from e

    def get_hidden_epics(self):
        """Get hidden epics for the project.

        :raises TimeoutError: When jira server connection timeout
        :raises NotFound: When jira server cannot find hidden epics
        :return: List of epics
        :rtype: List[Issue]
        """
        jql_str = f"project={self.project_id} AND issuetype=Epic AND labels IN ('hidden')"

        try:
            hidden_epics = self.jira.search_issues(
                jql_str=jql_str,
                maxResults=self.max_results,
                fields=self.fields["issues"],
            )
        except requests.exceptions.ReadTimeout as e:
            raise TimeoutError("Jira server connection timeout") from e
        except jira.exceptions.JIRAError as e:
            raise NotFound(e) from e
        return hidden_epics

    def get_issues(self):
        """Get issues for the project.

        :raises TimeoutError: When jira server connection timeout
        :raises NotFound: When jira server cannot find issues
        :return: List of issues
        :rtype: List[Issue]
        """

        hidden_epics = ",".join(epic.key for epic in self.get_hidden_epics())

        jql_str = (
            f"project={self.project_id} AND "
            + f"((issuetype IN ('Bug','Story Bug') AND KLANT IS NOT EMPTY) OR issuetype='Story') AND"
            + "(labels NOT IN ('hidden') OR labels IS EMPTY)"
        )

        if hidden_epics:
            jql_str = (
                jql_str
                + f" AND ('Epic Link' NOT IN ({hidden_epics}) OR 'Epic Link' IS EMPTY)"
            )

        issues = []
        start_at = 1
        c = 50
        try:
            while c:
                issues.extend(
                    self.jira.search_issues(
                        jql_str=jql_str,
                        maxResults=100,
                        fields=self.fields["issues"],
                        startAt=start_at,
                    )
                )
                start_at += 100
                c -= 1
        except requests.exceptions.ReadTimeout as e:
            raise TimeoutError("Jira server connection timeout") from e
        except jira.exceptions.JIRAError as e:
            raise NotFound(e) from e
        return issues[::-1]

    def get_epics(self):
        """Get epics for the project.

        :raises TimeoutError: When jira server connection timeout
        :raises NotFound: When jira server cannot find epics
        :return: List of epics
        :rtype: List[Issue]
        """
        jql_str = f"project={self.project_id} AND issuetype=Epic"

        try:
            epics = self.jira.search_issues(
                jql_str=jql_str,
                maxResults=self.max_results,
                fields=self.fields["issues"],
            )
        except requests.exceptions.ReadTimeout as e:
            raise TimeoutError("Jira server connection timeout") from e
        except jira.exceptions.JIRAError as e:
            raise NotFound(e) from e
        return epics
