class Release:
    """ Release entity """

    __slots__ = ["id", "type", "start_date", "release_date", "status"]

    def __init__(
        self, id: str, start_date: str, release_date: str, status: str,
    ):
        self.id = id
        self.type = "Release"
        self.start_date = start_date
        self.release_date = release_date
        self.status = status
