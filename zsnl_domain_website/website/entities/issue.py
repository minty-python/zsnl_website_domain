class Issue:
    """ Issue entity """

    __slots__ = [
        "id",
        "type",
        "public_description",
        "public_summary",
        "status",
        "epic",
        "releases",
    ]

    def __init__(
        self,
        id: str,
        public_description: str,
        public_summary: str,
        status: str,
        type: str,
        epic=None,
        releases=[],
    ):
        self.id = id
        self.type = type
        self.public_description = public_description
        self.public_summary = public_summary
        self.status = status
        self.epic = epic
        self.releases = releases
