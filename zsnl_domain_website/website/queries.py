from minty.cqrs import QueryBase


class Queries(QueryBase):
    """Queries class for Releases"""

    def get_latest_releases(self):
        """Get releases for an year.

        :return: list fo releases
        :rtype: list
        """
        return self.get_repository("release").get_latest()

    def get_issues(self, filter_params={}):
        """Get issues from the project.

        :param release: Release version of project
        :type release: str
        :return: list of issues
        :rtype: list
        """

        return self.get_repository("issue").get_issues(
            filter_params=filter_params
        )

    def search_issues(
        self, keyword: str, only_in_latest_releases: bool = True
    ):
        """Search in the issues of the project.

        :param keyword: keyword to search the issues
        :type keyword: str
        :param only_in_latest_releases: search only in latest releases if the value is True
        :type keyword: bool
        :return: list of issues
        :rtype: list
        """
        return self.get_repository("issue").search(
            keyword=keyword, only_in_latest_releases=only_in_latest_releases
        )
