from minty.cqrs import CommandBase


class Commands(CommandBase):
    """Queries class for Releases"""

    def update_cache(self):
        """Update cache for JIRA.
        """
        return self.get_repository("cache").update_cache()
