from minty.repository import Repository
from zsnl_domain_website import JiraRepositoryBase


class CacheRepository(Repository, JiraRepositoryBase):
    """ Cache repository """

    def update_cache(self):
        """Get all bugs from the active sprint which are not labelled hidden
        and reported by municipalities

        :return: list of bugs
        :rtype: list
        """

        jira = self._get_infrastructure("jira")

        releases = jira.get_releases()
        issues = jira.get_issues()
        epics = jira.get_epics()

        releases = [self._inflate_release(release) for release in releases]
        issues = [self._inflate_issue(issue) for issue in issues]
        epics = [self._inflate_epic(epic) for epic in epics]

        data = {"releases": releases, "issues": issues, "epics": epics}

        self.write_to_cache(data)

    def _inflate_release(self, release):
        if release.released:
            status = "released"
        elif release.archived:
            status = "archived"
        else:
            status = "unreleased"

        return {
            "id": release.name,
            "start_date": getattr(release, "startDate", None),
            "release_date": getattr(release, "releaseDate", None),
            "status": status,
        }

    def _inflate_issue(self, issue):
        """Create an instance of IssueEntity from JIRA issue.

        :param issue: Jira issue
        :type issue: JIRA.Issue
        :return: issue
        :rtype: Issue
        """
        issue = {
            "id": issue.key,
            "type": issue.fields.issuetype.name,
            "public_description": issue.fields.customfield_11300,
            "public_summary": issue.fields.customfield_11710,
            "status": issue.fields.status.name,
            "releases": [
                release.name
                for release in getattr(issue.fields, "fixVersions", [])
            ],
            "epic": issue.fields.customfield_10008,
        }
        return issue

    def _inflate_epic(self, epic):
        """Create an instance of IssueEntity from JIRA issue.

        :param issue: Jira issue
        :type issue: JIRA.Issue
        :return: issue
        :rtype: Issue
        """

        return {"id": epic.key, "name": epic.fields.customfield_10009}
