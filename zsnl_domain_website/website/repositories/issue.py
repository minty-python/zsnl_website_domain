import datetime
from ..entities import Issue
from dateutil.relativedelta import relativedelta
from minty.repository import Repository
from zsnl_domain_website import JiraRepositoryBase


class IssueRepository(Repository, JiraRepositoryBase):
    """Issue repository"""

    def get_issues(self, filter_params={}):
        """Get issues.

        :param filter_params: filter parameters
        :type filter_params: dict
        :return: list of issues
        :rtype: list
        """
        data = self.get_data_from_cache()
        issues = data["issues"]
        epics = data["epics"]

        if filter_params.get("relationships.release.id"):
            release = filter_params.get("relationships.release.id")
            issues = [
                issue for issue in issues if release in issue["releases"]
            ]

        for issue in issues:
            if issue["epic"]:
                issue["epic"] = next(
                    (epic for epic in epics if epic["id"] == issue["epic"]),
                    None,
                )

        return [Issue(**issue) for issue in issues]

    def search(self, keyword: str, only_in_latest_releases: bool = True):
        """Search issues by keyword.

        :param keyword: keyword
        :type keyword: str
        :param only_in_latest_releases: search only in latest releases if the value is True
        :type keyword: bool
        :return: list of issues which matches the keyword
        :rtype: list
        """
        data = self.get_data_from_cache()
        issues = data["issues"]
        epics = data["epics"]
        releases = data["releases"]

        # Get issues from latest releases
        if only_in_latest_releases:
            latest_releases = [
                release["id"]
                for release in releases
                if (
                    release["release_date"]
                    and datetime.datetime.strptime(
                        release["release_date"], "%Y-%m-%d"
                    )
                    > datetime.datetime.today() - relativedelta(months=+12)
                )
            ]
            issues = [
                issue
                for issue in issues
                if (
                    issue["releases"]
                    and set(issue["releases"]).issubset(latest_releases)
                )
            ]

        for issue in issues:
            if issue["epic"]:
                issue["epic"] = next(
                    (epic for epic in epics if epic["id"] == issue["epic"]),
                    None,
                )

        # Search issues by id, public_description, public_summary and epic_name
        # Search on data with or without a capital letter, so no need to take spelling into account
        keyword = keyword.lower()
        issues = [
            issue
            for issue in issues
            if (
                (keyword in issue["id"].lower())
                or (
                    issue["public_description"]
                    and keyword in issue["public_description"].lower()
                )
                or (
                    issue["public_summary"]
                    and keyword in issue["public_summary"].lower()
                )
                or (issue["epic"] and keyword in issue["epic"]["name"].lower())
            )
        ]

        return [Issue(**issue) for issue in issues]
