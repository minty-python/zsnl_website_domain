import datetime
from ..entities import Release
from dateutil.relativedelta import relativedelta
from minty.repository import Repository
from zsnl_domain_website import JiraRepositoryBase


class ReleaseRepository(Repository, JiraRepositoryBase):
    """Releases repository """

    def get_latest(self):
        """Get releases of the project for last one year

        :return: list releases of the project for last one year
        :rtype: list
        """
        data = self.get_data_from_cache()
        releases = data["releases"]

        last_year_same_day = datetime.datetime.today() - relativedelta(
            months=+12
        )

        filtered_releases = filter(
            lambda x: (
                x["release_date"]
                and datetime.datetime.strptime(x["release_date"], "%Y-%m-%d")
                > last_year_same_day
            ),
            releases,
        )

        sorted_releases = sorted(
            filtered_releases, key=lambda i: i["release_date"], reverse=True
        )

        return [Release(**release) for release in sorted_releases]
