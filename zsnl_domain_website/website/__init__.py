from .commands import Commands
from .queries import Queries
from .repositories import CacheRepository, IssueRepository, ReleaseRepository

REQUIRED_REPOSITORIES = {
    "cache": CacheRepository,
    "release": ReleaseRepository,
    "issue": IssueRepository,
}


def get_query_instance(repository_factory, context, user_uuid):
    return Queries(
        repository_factory=repository_factory,
        context=context,
        user_uuid=user_uuid,
    )


def get_command_instance(
    repository_factory, context, user_uuid, event_service
):
    return Commands(repository_factory, context, user_uuid, event_service)
