__version__ = "0.0.21"

import datetime
import json
import os
import tempfile
from minty import Base
from minty.exceptions import NotFound
from minty.infrastructure import InfrastructureFactory
from uuid import UUID
from zsnl_domain_website.infrastructure.jira import JiraInfrastructure


class QueryBase(Base):
    """Base class for query classes"""

    __slots__ = ["repository_factory", "context", "user_uuid"]

    def __init__(self, repository_factory, context, user_uuid: UUID):
        self.repository_factory = repository_factory
        self.context = context
        self.user_uuid = user_uuid


class CommandBase(Base):
    __slots__ = ["repository_factory", "context", "user_uuid", "event_service"]

    def __init__(
        self, repository_factory, context, user_uuid, event_service=None,
    ):
        self.repository_factory = repository_factory
        self.context = context
        self.user_uuid = user_uuid
        self.event_service = event_service


class JiraRepositoryBase(Base):
    """Base class for Database repository"""

    __slots__ = ["infrastructure_factory", "context"]

    REQUIRED_INFRASTRUCTURE = {"jira": JiraInfrastructure()}

    temp_folder = tempfile.gettempdir()
    cache_file_path = os.path.join(temp_folder, "jira_cache.txt")

    def __init__(
        self, infrastructure_factory: InfrastructureFactory, context: str
    ):
        self.infrastructure_factory = infrastructure_factory
        self.context = context

    def _get_infrastructure(self, name):
        """Get infrastrucute

        :param name: name of infrastructure
        :type name: str
        :return: infrastructure
        :rtype: Infrastructure
        """

        return self.infrastructure_factory.get_infrastructure(
            context=self.context, infrastructure_name=name
        )

    def get_data_from_cache(self):
        """Get data from cache(cache_file_path).

        :raises NotFound: When cache file cannot be found
        :return: cached data as a dict
        :rtype: dict
        """
        try:
            with open(self.cache_file_path, "r") as cache:
                return json.loads(cache.read())
        except FileNotFoundError:
            raise NotFound("Page is broken. Try again after 30 mintues.")

    def write_to_cache(self, data: dict):
        """Write the data in cache(in cache_file_path).

        :param data: data needed to be written in cache file
        :type name: dict
        """
        timestamp = datetime.datetime.now().isoformat()
        temp_file = f"jira_cache_{timestamp}.txt"
        temp_file_path = os.path.join(self.temp_folder, temp_file)

        # To make sure that the process of updating the cache is atomic,
        # we are first copying data to tempfile, and then rename temp file to  cache file.
        with open(temp_file_path, "w+") as temp:
            temp.write(json.dumps(data))

        os.rename(temp_file_path, self.cache_file_path)
