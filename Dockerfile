### Stage 1: Setup base python with requirements
FROM python:3.7-slim-stretch AS base-layer

# Add necessary packages to system
RUN apt-get update && apt-get install -y git && apt-get clean

# Temp location: make sure we cache this bitch
RUN pip install ipython

# Copy requirements before copying complete source, to allow caching
COPY requirements/base.txt /tmp
RUN cd /tmp && pip install -r base.txt

# Set up application
COPY . /opt/zsnl_domain_website
WORKDIR /opt/zsnl_domain_website

FROM base-layer AS production

ENV OTAP=production

ENV PYTHONUNBUFFERED=on

FROM base-layer AS quality-and-testing

WORKDIR /tmp
COPY requirements/test.txt /tmp

RUN pip install -r test.txt

WORKDIR /opt/zsnl_domain_website

RUN bin/generate_documentation.sh

FROM quality-and-testing AS development

ENV OTAP=development
